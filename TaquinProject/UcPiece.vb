﻿Public Class UcPiece

    Public Delegate Sub OnLabelClick(sender As Object, e As EventArgs)
    Public Event LabelClickEventHandler As OnLabelClick
    Public Property Number As Short = 0
    Private m_WithPicture As Boolean

    Public Sub New(Optional ByVal WithPicture As Boolean = False)
        m_WithPicture = WithPicture
        InitializeComponent()
    End Sub

    Private Sub UcPiece_Click(sender As Object, e As EventArgs) Handles MyBase.Click
        RaiseEvent LabelClickEventHandler(Me, e)
    End Sub

End Class
