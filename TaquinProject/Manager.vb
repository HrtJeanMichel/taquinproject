﻿Imports System.Threading

Public Class Manager

    Private m_Tlp As TableLayoutPanel
    Private m_Random As Random
    Private ThreadLoadPieces As Thread
    Private Delegate Sub UpdateStatusDelegate(uPiece As UcPiece)
    Public Delegate Sub OnRandomTerminated(sender As Object, e As EventArgs)
    Public Event RandomTerminatedEventHandler As OnRandomTerminated

    Private m_WithPicture As Boolean

    Public Sub New(Tlp As TableLayoutPanel)
        m_Tlp = Tlp
        m_Random = New Random
        PiecesRandomize()
    End Sub

    Public Sub PiecesRandomize(Optional ByVal WithPicture As Boolean = False)
        m_WithPicture = WithPicture
        m_Tlp.Controls.Clear()
        ThreadLoadPieces = New Thread(AddressOf InitPiecesRandomize)
        ThreadLoadPieces.IsBackground = True
        ThreadLoadPieces.Start()
    End Sub

    Private Sub InitPiecesRandomize()
        Try
            Dim larg = 4
            Dim haut = 4
            Dim cases As Short = larg * haut
            Dim permut As Short
            Dim parite As Short
            Dim grid As Short()
            Dim tempGrid As Short()

            Do
                grid = New Short(cases - 1) {}
                tempGrid = New Short(cases - 1) {}

                Dim i As Short
                Dim j As Short
                Dim x As Short
                Dim tmp As Short

                parite = 0
                permut = 0

                For i = 0 To cases - 1
                    grid(i) = i
                Next

                For i = 0 To cases - 1
                    x = i + (m_Random.Next() Mod (cases - i))
                    tmp = grid(i)
                    grid(i) = grid(x)
                    grid(x) = tmp
                Next

                i = 0

                While grid(i) <> 0
                    i += 1
                End While

                parite = (larg - 1) + (haut - 1) - ((i / larg) + (i Mod larg))
                Array.Copy(grid, tempGrid, cases - 1)

                If i < cases - 1 Then
                    tmp = tempGrid(cases - 1)
                    tempGrid(cases - 1) = tempGrid(i)
                    tempGrid(i) = tmp
                    permut += 1
                End If

                For i = cases - 1 To 1 Step -1
                    If tempGrid(i - 1) < i Then
                        For j = i - 1 To 0 Step -1
                            If tempGrid(j) = i Then
                                tmp = tempGrid(j)
                                tempGrid(j) = tempGrid(i - 1)
                                tempGrid(i - 1) = tmp
                                permut += 1
                            End If
                        Next
                    End If
                Next
            Loop While ((permut Mod 2) Xor (parite Mod 2))

            If m_WithPicture Then
                Dim img = My.Resources._01
                Dim Number As Short
                For r As Short = 0 To 4 - 1
                    For c As Short = 0 To 4 - 1
                        Dim index = r * larg + c
                        Number = grid(index)
                        Dim uPiece As New UcPiece(True)
                        uPiece.Number = Number
                        With uPiece
                            .Name = "UcPiece_" & (Number).ToString
                            If Number = 0 Then
                                .BackColor = Color.Transparent
                            Else
                                .BackgroundImage = GetPieceImage(img, GetPieceSize(Number))
                            End If
                        End With
                        UpdateStatus(uPiece)
                    Next
                Next
            Else
                For k As Short = 0 To cases - 1
                    Dim num = grid(k)
                    Dim uPiece As New UcPiece
                    uPiece.Number = num
                    With uPiece
                        .Name = "UcPiece_" & (num).ToString
                        .BackgroundImage = GetPieceNumber(num, uPiece.Size)
                    End With
                    UpdateStatus(uPiece)
                Next
            End If

            RaiseEvent RandomTerminatedEventHandler(Me, EventArgs.Empty)
        Catch ex As Exception
        End Try
    End Sub

    Private Function GetPieceImage(original_image As Bitmap, pt As Point) As Bitmap
        Dim rect As Rectangle = New Rectangle(pt.X, pt.Y, 100, 100)
        Dim timage As Bitmap
        timage = New Bitmap(100, 100)
        timage = original_image.Clone(rect, original_image.PixelFormat)
        Return timage
    End Function

    Private Function GetPieceNumber(uPieceNum As Short, uPieceSize As Size) As Bitmap

        Dim offScreenBmp = New Bitmap(uPieceSize.Width, uPieceSize.Height)
        Dim offScreenDC As Graphics = Graphics.FromImage(offScreenBmp)

        offScreenDC.FillRectangle(If(uPieceNum = 0, Brushes.White, Brushes.DarkCyan), offScreenDC.ClipBounds)
        Dim font As Font = New Font(FontFamily.GenericSansSerif, 12, FontStyle.Regular)
        Dim size As SizeF = offScreenDC.MeasureString(uPieceNum.ToString(), font)
        If uPieceNum > 0 Then
            offScreenDC.DrawString(uPieceNum.ToString, font, New SolidBrush(Color.White), (uPieceSize.Width - size.Width) / 2, (uPieceSize.Height - size.Height) / 2)
        End If
        Return offScreenBmp
    End Function

    Private Function GetPieceSize(number As Short) As Point
        Select Case number
            Case 1
                Return New Point(0 * 100, 0 * 100)
            Case 2
                Return New Point(1 * 100, 0 * 100)
            Case 3
                Return New Point(2 * 100, 0 * 100)
            Case 4
                Return New Point(3 * 100, 0 * 100)
            Case 5
                Return New Point(0 * 100, 1 * 100)
            Case 6
                Return New Point(1 * 100, 1 * 100)
            Case 7
                Return New Point(2 * 100, 1 * 100)
            Case 8
                Return New Point(3 * 100, 1 * 100)
            Case 9
                Return New Point(0 * 100, 2 * 100)
            Case 10
                Return New Point(1 * 100, 2 * 100)
            Case 11
                Return New Point(2 * 100, 2 * 100)
            Case 12
                Return New Point(3 * 100, 2 * 100)
            Case 13
                Return New Point(0 * 100, 3 * 100)
            Case 14
                Return New Point(1 * 100, 3 * 100)
            Case 15
                Return New Point(2 * 100, 3 * 100)
        End Select
    End Function

    Private Sub UpdateStatus(uPiece As UcPiece)
        Try
            If m_Tlp.InvokeRequired Then
                Dim dlg As New UpdateStatusDelegate(AddressOf UpdateStatus)
                m_Tlp.Parent.Invoke(dlg, uPiece)

            Else
                m_Tlp.Controls.Add(uPiece)
                If uPiece.Number > 0 Then
                    AddHandler uPiece.LabelClickEventHandler, AddressOf LabelClick
                End If
            End If
        Catch ex As ObjectDisposedException
        End Try
    End Sub

    Private Sub LabelClick(sender As Object, e As EventArgs)
        Dim UcEmpty = m_Tlp.Controls.Cast(Of UcPiece).Where(Function(f) f.Number = 0).FirstOrDefault()
        Dim UcEmptyLoc = m_Tlp.GetPositionFromControl(UcEmpty)

        Dim UcCurrentLoc = m_Tlp.GetPositionFromControl(TryCast(sender, UcPiece))

        If UcEmptyLoc.Row = UcCurrentLoc.Row Then
            If UcCurrentLoc.Column > UcEmptyLoc.Column Then
                For i As Integer = UcEmptyLoc.Column To UcCurrentLoc.Column - 1
                    SwapControls(m_Tlp.GetControlFromPosition(i, UcEmptyLoc.Row), m_Tlp.GetControlFromPosition(i + 1, UcEmptyLoc.Row))
                Next
            ElseIf UcCurrentLoc.Column < UcEmptyLoc.Column Then
                For i As Integer = UcEmptyLoc.Column To UcCurrentLoc.Column + 1 Step -1
                    SwapControls(m_Tlp.GetControlFromPosition(i, UcEmptyLoc.Row), m_Tlp.GetControlFromPosition(i - 1, UcEmptyLoc.Row))
                Next
            End If
        End If

        If UcEmptyLoc.Column = UcCurrentLoc.Column Then
            If UcCurrentLoc.Row > UcEmptyLoc.Row Then
                For i As Integer = UcEmptyLoc.Row To UcCurrentLoc.Row - 1
                    SwapControls(m_Tlp.GetControlFromPosition(UcEmptyLoc.Column, i), m_Tlp.GetControlFromPosition(UcEmptyLoc.Column, i + 1))
                Next
            ElseIf UcCurrentLoc.Row < UcEmptyLoc.Row Then
                For i As Integer = UcEmptyLoc.Row To UcCurrentLoc.Row + 1 Step -1
                    SwapControls(m_Tlp.GetControlFromPosition(UcEmptyLoc.Column, i), m_Tlp.GetControlFromPosition(UcEmptyLoc.Column, i - 1))
                Next
            End If
        End If
    End Sub

    Private Sub SwapControls(uc1 As UcPiece, uc2 As UcPiece)
        m_Tlp.SuspendLayout()
        Dim ctl1pos As TableLayoutPanelCellPosition = m_Tlp.GetPositionFromControl(uc1)
        m_Tlp.SetCellPosition(uc1, m_Tlp.GetPositionFromControl(uc2))
        m_Tlp.SetCellPosition(uc2, ctl1pos)
        m_Tlp.ResumeLayout()
    End Sub

End Class
