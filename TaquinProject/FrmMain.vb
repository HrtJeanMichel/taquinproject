﻿Public Class FrmMain

    Private m_Manager As Manager
    Private Delegate Sub UpdateButtonDelegate(state As Boolean)

    Public Sub New()
        InitializeComponent()
        m_Manager = New Manager(TlpMain)
        AddHandler m_Manager.RandomTerminatedEventHandler, AddressOf RandomTerminated
    End Sub

    Private Sub BtnGenerateNumberGrid_Click(sender As Object, e As EventArgs) Handles BtnGenerateNumberGrid.Click
        ButtonStatus(False)
        m_Manager.PiecesRandomize()
    End Sub

    Private Sub BtnGenerateImageGrid_Click(sender As Object, e As EventArgs) Handles BtnGenerateImageGrid.Click
        ButtonStatus(False)
        m_Manager.PiecesRandomize(True)
    End Sub

    Private Sub RandomTerminated(sender As Object, e As EventArgs)
        ButtonStatus(True)
    End Sub

    Private Sub ButtonStatus(state As Boolean)
        If SpcMain.Panel1.InvokeRequired Then
            SpcMain.Panel1.Invoke(New UpdateButtonDelegate(AddressOf ButtonStatus), state)
        Else
            SpcMain.Panel1.Enabled = state
        End If
    End Sub
End Class
