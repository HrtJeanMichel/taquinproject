﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmMain
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Prevent Child Controls Flicker
    Protected Overrides ReadOnly Property CreateParams As CreateParams
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ExStyle = cp.ExStyle Or &H2000000
            Return cp
        End Get
    End Property

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMain))
        Me.TlpMain = New System.Windows.Forms.TableLayoutPanel()
        Me.BtnGenerateNumberGrid = New System.Windows.Forms.Button()
        Me.BtnGenerateImageGrid = New System.Windows.Forms.Button()
        Me.SpcMain = New System.Windows.Forms.SplitContainer()
        CType(Me.SpcMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SpcMain.Panel1.SuspendLayout()
        Me.SpcMain.Panel2.SuspendLayout()
        Me.SpcMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'TlpMain
        '
        Me.TlpMain.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TlpMain.ColumnCount = 4
        Me.TlpMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TlpMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TlpMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TlpMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TlpMain.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize
        Me.TlpMain.Location = New System.Drawing.Point(0, 2)
        Me.TlpMain.Margin = New System.Windows.Forms.Padding(0)
        Me.TlpMain.Name = "TlpMain"
        Me.TlpMain.RowCount = 4
        Me.TlpMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TlpMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TlpMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TlpMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TlpMain.Size = New System.Drawing.Size(400, 400)
        Me.TlpMain.TabIndex = 0
        '
        'BtnGenerateNumberGrid
        '
        Me.BtnGenerateNumberGrid.Location = New System.Drawing.Point(3, 3)
        Me.BtnGenerateNumberGrid.Name = "BtnGenerateNumberGrid"
        Me.BtnGenerateNumberGrid.Size = New System.Drawing.Size(194, 23)
        Me.BtnGenerateNumberGrid.TabIndex = 1
        Me.BtnGenerateNumberGrid.Text = "Générer grille 4x4 sans image"
        Me.BtnGenerateNumberGrid.UseVisualStyleBackColor = True
        '
        'BtnGenerateImageGrid
        '
        Me.BtnGenerateImageGrid.Location = New System.Drawing.Point(203, 3)
        Me.BtnGenerateImageGrid.Name = "BtnGenerateImageGrid"
        Me.BtnGenerateImageGrid.Size = New System.Drawing.Size(194, 23)
        Me.BtnGenerateImageGrid.TabIndex = 2
        Me.BtnGenerateImageGrid.Text = "Générer grille 4x4 avec image"
        Me.BtnGenerateImageGrid.UseVisualStyleBackColor = True
        '
        'SpcMain
        '
        Me.SpcMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SpcMain.IsSplitterFixed = True
        Me.SpcMain.Location = New System.Drawing.Point(12, 12)
        Me.SpcMain.Name = "SpcMain"
        Me.SpcMain.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SpcMain.Panel1
        '
        Me.SpcMain.Panel1.Controls.Add(Me.BtnGenerateNumberGrid)
        Me.SpcMain.Panel1.Controls.Add(Me.BtnGenerateImageGrid)
        '
        'SpcMain.Panel2
        '
        Me.SpcMain.Panel2.Controls.Add(Me.TlpMain)
        Me.SpcMain.Size = New System.Drawing.Size(402, 435)
        Me.SpcMain.SplitterDistance = 29
        Me.SpcMain.TabIndex = 3
        '
        'FrmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(426, 459)
        Me.Controls.Add(Me.SpcMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmMain"
        Me.Text = "Taquin"
        Me.SpcMain.Panel1.ResumeLayout(False)
        Me.SpcMain.Panel2.ResumeLayout(False)
        CType(Me.SpcMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SpcMain.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TlpMain As TableLayoutPanel
    Friend WithEvents BtnGenerateNumberGrid As Button
    Friend WithEvents BtnGenerateImageGrid As Button
    Friend WithEvents SpcMain As SplitContainer
End Class
